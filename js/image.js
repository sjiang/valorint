// from https://github.com/processing/p5.js/blob/main/src/image/filters.js
function thresholdFilter(pixels, level) {
    if (level === undefined) {
        level = 0.5;
    }
    const thresh = Math.floor(level * 255);
    for (let i = 0; i < pixels.length; i += 4) {
        const r = pixels[i];
        const g = pixels[i + 1];
        const b = pixels[i + 2];
        const gray = 0.2126 * r + 0.7152 * g + 0.0722 * b;
        let val;
        if (gray >= thresh) {
            val = 255;
        } else {
            val = 0;
        }
        pixels[i] = pixels[i + 1] = pixels[i + 2] = val;
    }
}

function redFilter(pixels) {
    const redThreshMin = 150;
    const greenThreshMax = 100;
    const blueThreshMax = 100;
    for (let i = 0; i < pixels.length; i += 4) {
        const r = pixels[i];
        const g = pixels[i + 1];
        const b = pixels[i + 2];
        if (r >= redThreshMin &&
            g <= greenThreshMax &&
            b <= blueThreshMax) {
            pixels[i] = pixels[i + 1] = pixels[i + 2] = 255;
        }
    }
}

function invertColors(pixels) {
    for (var i = 0; i < pixels.length; i+= 4) {
        pixels[i] = pixels[i] ^ 255; // Invert Red
        pixels[i+1] = pixels[i+1] ^ 255; // Invert Green
        pixels[i+2] = pixels[i+2] ^ 255; // Invert Blue
    }
}
