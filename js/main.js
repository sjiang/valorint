
const ocrHeaders = ['Team', 'Agent', 'Player','Combat Score','Kill','Death','Assist','Econ','First Blood','Plant','Defuse'];

const matchHeaders = ['Date', 'Map', 'Team 1', 'Team 2','Detail'];

const agents = ['', 'Brimstone', 'Viper', 'Omen', 'Killjoy', 'Cypher', 'Sova', 'Sage', 'Phoenix', 'Jett', 'Reyna', 'Raze', 'Breach', 'Skye', 'Yoru', 'Astra'].sort()

const maps = ['', 'Ascent', 'Bind', 'Haven', 'Split', 'Icebox'].sort();

/**
 * Reads OCR output and coverts into JSON
 * 
 * Expected OCR input format comes from an image with agents cropped out. Example below
 * 
 * 13 VICTORY 7
 * SUMMARY SCOREBOARD TIMELINE PERFORMANCE
 * # MAP - BSTENT
 * INDIVIDUALLY SORTED AVG COMBAT SCORE KDA ECON RATING FIRST BLOODS PLANTS DEFUSES
 * EnormousPenguin 284 20 16 3 70 3 2 0
 * zgmdfg 113 8 16 5 35 0 0 0
 * 
 * @param {OCR detected text} detectedText 
 * @returns {
 *  matchResult,
 *  map,
 *  stats: {
 *      [username]: [
 *          combatScore,
 *          kills,
 *          deaths,
 *          assists,
 *          econRating,
 *          firstBloods,
 *          plants,
 *          defuses
 *      ]
 *  }
 * }
 */
function readOCRWithText(detectedText) {
    const detectedLines = detectedText.split('\n');

    const matchResult = detectedLines.shift();
    const tabs = detectedLines.shift(); /* ignored */
    const map = detectedLines.shift().split(' ').pop();
    const headers = detectedLines.shift(); /* ignored */
    
    const stats = new Map();
    // At this point, we should only have usernames and stats remaining
    for (const user of detectedLines) {
        // reverse the stats so we end with a username after we get the expected stats.
        const userStats = user.split(' ');

        // data is expected in certain form, so pop expected values
        const defuses = userStats.pop();
        const plants = userStats.pop();
        const firstBloods = userStats.pop();
        const econRating = userStats.pop();
        const assists = userStats.pop();
        const deaths = userStats.pop();
        const kills = userStats.pop();
        const combatScore = userStats.pop();
        const name = userStats.join(' ');

        stats.set(name, [combatScore, kills, deaths, assists, econRating, firstBloods, plants, defuses]);
    }

    return { matchResult, map: findBestWordMatch(map, maps), stats };
}

function findBestWordMatch(ocrText, guesses) {
    const matchPercent = [];
    for (const guess of guesses) {
        matchPercent.push(similarity(ocrText, guess))
    }

    return guesses[matchPercent.indexOf(Math.max(...matchPercent))];
}

// Levenshtein distance https://en.wikipedia.org/wiki/Levenshtein_distance
function similarity(s1, s2) {
  var longer = s1;
  var shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  var longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}

function editDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  var costs = new Array();
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0)
        costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}

function readOCR() {
    str = document.getElementById("ocrTextArea").value
    let result = str.split('\n').map(s=>['',''].concat(s.split(' ')))
    let table = document.getElementById("ocrTable");
    table.innerHTML=""
    generateTable(table, result);
    generateTableHead(table, ocrHeaders);
    $("#readOCRButton").removeClass('primary');
    $("#readOCRButton").addClass('outline');
    $("#generateJSONButton").removeClass('outline');
    $("#generateJSONButton").addClass('primary');
}

function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}

function generateTableFromData(table, ocrData) {
    for (let [name, stats] of ocrData.entries()) {
        stats.unshift('','', '');
        addTableRow(table, stats, name);
    }
}

function addTableRow(table, stats, name) {
    let row = table.insertRow();
    for (let index = 0; index < ocrHeaders.length; index++) {
        let newElement;
        if (index === 0) {
            newElement = document.createElement('input');
            newElement.type = 'checkbox';
        }
        else if (index === 1){
            newElement = document.createElement('select');
            newElement.style = 'min-width: 100px'
            agents.forEach(function (agent) {
                var option = document.createElement("option");
                option.value = agent;
                option.text = agent;
                newElement.appendChild(option);
            });
        }
        else if (ocrHeaders[index] === 'Player') {
            newElement = document.createElement('input');
            newElement.type = 'text';
            newElement.style = 'min-width: 200px'
            newElement.value = name ? name : stats[index]==undefined?'':stats[index];;
        }
        else {
            newElement = document.createElement('input');
            newElement.type = 'number';
            newElement.value = stats[index] == undefined ? '' : stats[index];
        }
        if (index < ocrHeaders.length) {
            let cell = row.insertCell();
            cell.appendChild(newElement);
        }
    }
}
function generateTable(table, data) {
    for (let element of data) {
        addTableRow(table, element);
    }
    $('tr').find('input').on('click', function() {
        if ($(this).prop('checked') === true) {
           $(this).closest('tr').addClass('win'); 
        } else {
           $(this).closest('tr').removeClass('win'); 
        }
    });
}


function generateJSON() {
    let tableRows = $("#ocrTable tr");
    if (tableRows.length == 0) {
        alert("No records! Please Parse Input first")
        return;
    }
    rows = [];
    roundUnchecked = parseInt($("#rounds1")[0].value);
    roundChecked = parseInt($("#rounds2")[0].value);
    let jsonDiv = document.getElementById("convertedJSON");
    jsonDiv.innerHTML="";

    tableRows.each(function (rowIndex, tableRow) {
        if (rowIndex > 0){
            let row = {};
            tableCells = tableRow.children;
            row['Map'] = $("#matchMap")[0].value;
            row['Date'] = $("#matchDate")[0].value;
            if (tableCells[0].children[0].checked === true){
                row['Rounds Won'] = roundChecked;
            } else {
                row['Rounds Won'] = roundUnchecked;
            }
            Array.from(tableCells).forEach(function (tableCell, cellIndex){
                if (cellIndex > 0){
                    if (['Player', 'Agent'].includes(ocrHeaders[cellIndex])){
                        row[ocrHeaders[cellIndex]] = tableCell.children[0].value;
                    } else {
                        row[ocrHeaders[cellIndex]] = parseInt(tableCell.children[0].value);
                    }
                    
                }
            });
            rows.push(row);
        }
    });
    let newElement = document.createElement('textarea');
    newElement.value = JSON.stringify(rows);
    newElement.style = 'min-height:400px';
    jsonDiv.appendChild(newElement);
    $("#readOCRButton").removeClass('outline');
    $("#readOCRButton").addClass('primary');
    $("#generateJSONButton").removeClass('primary');
    $("#generateJSONButton").addClass('outline');
}

function openPage(pageName, elmnt, color) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Remove the background color of all tablinks/buttons
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.borderBottom = "2px solid var(--color-lightGrey)";
    }

    // Show the specific tab content
    document.getElementById(pageName).style.display = "block";
    elmnt.style.borderBottom = "2px solid var(--color-success)";
}


function pad(value, length) {
    return (value.toString().length < length) ? pad("0"+value, length):value;
}

function preprocessStats(json) {
    json.forEach(function (match, recordIndex){
        let winningRounds = Math.max.apply(Math, match.map(function(o) {return o["Rounds Won"];}));
        let losingRounds = Math.min.apply(Math, match.map(function(o) {return o["Rounds Won"];}));
        let totalRounds = winningRounds + losingRounds;
        let totalPlayers = match.length;
        let imageURL = "images/rounds/"+pad(recordIndex, 4)+".png"
        match.forEach(function (record){
            record["Winning Rounds"] = winningRounds;
            record["Total Rounds"] = totalRounds;
            record["Image URL"] = imageURL;
        });
    });
    return json;
}

function generateChartData(json, jsonFlattened){
    let output = {};
    jsonFlattened.forEach(function (record){
        if (!output.hasOwnProperty(record.Player)){
            output[record.Player] = {
                'Combat Scores' : [],
                'Kills' : [],
                'Deaths' : [],
                'Assists' : [],
                'KDAs' : [],
                'Kills per Round' : [],
                'Deaths per Round' : [],
                'Assists per Round' : [],
                'Econ Ratings' : [],
                'First Bloods' : [],
                'Plants' : [],
                'Defuses' : [],
            }
        }
        output[record.Player]['Combat Scores'].push(record["Combat Score"]);
        output[record.Player]['Kills'].push(record["Kill"]);
        output[record.Player]['Kills per Round'].push(record["Kill"]/record["Total Rounds"]);
        output[record.Player]['Deaths per Round'].push(record["Death"]/record["Total Rounds"]);
        output[record.Player]['Assists per Round'].push(record["Assist"]/record["Total Rounds"]);
        output[record.Player]['Deaths'].push(record["Death"]);
        output[record.Player]['Assists'].push(record["Assist"]);
        output[record.Player]['Econ Ratings'].push(record["Econ"]);
        output[record.Player]['First Bloods'].push(record["First Blood"]/record["Total Rounds"]);
        output[record.Player]['Plants'].push(record["Plant"]/record["Total Rounds"]);
        output[record.Player]['Defuses'].push(record["Defuse"]/record["Total Rounds"]);
        output[record.Player]['KDAs'].push((record["Kill"]+record["Assist"]/3)/record["Death"]);
    });
    Object.keys(output).forEach(function(player){
        output[player]['Quartiles'] = {
            'Combat Scores': [
                q25(output[player]['Combat Scores']), 
                q50(output[player]['Combat Scores']),
                q75(output[player]['Combat Scores'])
            ],
            'KDAs': [
                q25(output[player]['KDAs']), 
                q50(output[player]['KDAs']),
                q75(output[player]['KDAs'])
            ],
            'Econ Ratings': [
                q25(output[player]['Econ Ratings']), 
                q50(output[player]['Econ Ratings']),
                q75(output[player]['Econ Ratings'])
            ],
            'First Bloods': [
                q25(output[player]['First Bloods']), 
                q50(output[player]['First Bloods']),
                q75(output[player]['First Bloods'])
            ],
            'Plants': [
                q25(output[player]['Plants']), 
                q50(output[player]['Plants']),
                q75(output[player]['Plants'])
            ],
            'Defuses': [
                q25(output[player]['Defuses']), 
                q50(output[player]['Defuses']),
                q75(output[player]['Defuses'])
            ],
        };
        output[player]['Round Winrate'] = sum(jsonFlattened.filter(elem => elem.Player==player).map(function(item){return item["Rounds Won"]}))/sum(jsonFlattened.filter(elem => elem.Player==player).map(function(item){return item["Total Rounds"]}));
        output[player]['Match Winrate'] = jsonFlattened.filter(elem => elem.Player==player && elem["Rounds Won"] == elem["Winning Rounds"]).length/jsonFlattened.filter(elem => elem.Player==player).length;
    });
    return output
}

function generateMatchTable(json){
    let table = document.getElementById("matchTable");
    table.innerHTML=""
    generateMatchTableRecords(table, json);
    generateTableHead(table, matchHeaders.slice(0,matchHeaders.length-1));
    sorttable.makeSortable(table);
}

function generateMatchTableRecords(table, data){
    data.reverse().forEach(function( element, roundNumber) {
        //['Date', 'Map', 'Team 1', 'Team 2', 'Detail'];
        let row = table.insertRow();
        row.setAttribute('onclick', 'toggleRow(this)');
        matchHeaders.forEach(function (item, index) {
            let newElement
            if (matchHeaders[index] === 'Date') {
                newElement = document.createTextNode(element[0]["Date"]);
            }
            else if (matchHeaders[index] === 'Map'){
                newElement = document.createTextNode(element[0]["Map"]);
            }
            else if (matchHeaders[index] === 'Team 1') {
                newElement = document.createElement('p');
                newElement.classList = "team";
                let winningRounds = element[0]["Winning Rounds"];
                newElement.innerHTML = winningRounds + "<br />";
                let newSpan = document.createElement("span");
                newSpan.className = 'agents';
                
                let team = element.filter(function(val,ind){return val["Rounds Won"]==winningRounds});
                team.forEach(function(member){
                    let newImg = document.createElement("img");
                    newImg.src = 'images/agents/' + member["Agent"] + '_icon.webp';
                    newSpan.appendChild(newImg);
                })

                newElement.appendChild(newSpan);
            }
            else if (matchHeaders[index] === 'Team 2') {
                newElement = document.createElement('p');
                newElement.classList = "team";
                losingRounds = element[0]["Total Rounds"] - element[0]["Winning Rounds"];
                newElement.innerHTML = losingRounds + "<br />";
                let newSpan = document.createElement("span");
                newSpan.className = 'agents';
                
                let team = element.filter(function(val,ind){return val["Rounds Won"]==losingRounds});
                team.forEach(function(member){
                    let newImg = document.createElement("img");
                    newImg.src = 'images/agents/' + member["Agent"] + '_icon.webp';
                    newSpan.appendChild(newImg);
                })
                
                newElement.appendChild(newSpan);
            }
            else if (matchHeaders[index] === 'Detail') {
                newElement = document.createElement('img');
                newElement.src = element[0]["Image URL"];
            }
            else {
                newElement = document.createTextNode('');
            }
            let cell = row.insertCell();
            if (matchHeaders[index] === 'Detail') {
                cell.classList = 'expanded-row-content hide-row';
            }
            cell.appendChild(newElement);
        });
    });
}

function highlightParseImage() {
    $("#generateOCRButton").removeClass('outline');
    $("#generateOCRButton").addClass('primary');
}

function previewMatchImage() {
    const image = new Image();
    const input = document.getElementById('matchUpload');
    image.src = URL.createObjectURL(input.files[0]);
    image.onload = () => {
        verifyImage(image);
    }
}

function verifyImage(image) {
    // draw image on canvas
    const canvas = document.getElementById('ocrCanvas');
    canvas.width = image.width;
    canvas.height = image.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(image, 0, 0);

    // preprocess image
    const processedImage = preprocessImage(canvas);
    ctx.putImageData(processedImage, 0, 0);

    // run through tesseract ocr
    Tesseract.recognize(canvas.toDataURL('image/png'))
        .then(({ data: { text } }) => {
            const ocrData = readOCRWithText(text);
            
            const table = document.getElementById('ocrTable');
            table.innerHTML=""
            generateTableFromData(table, ocrData.stats);
            generateTableHead(table, ocrHeaders);

            const splitMatchResult = ocrData.matchResult.split(' ');
            const rounds1 = document.getElementById('rounds1');
            rounds1.value = splitMatchResult[0];
            const rounds2 = document.getElementById('rounds2');
            rounds2.value = splitMatchResult[2];

            const matchMap = document.getElementById('matchMap');
            matchMap.value = ocrData.map;

            $("#generateJSONButton").removeClass('outline');
            $("#generateJSONButton").addClass('primary');
        });
}

function preprocessImage(canvas) {
    const processedImageData = canvas.getContext('2d').getImageData(0,0,canvas.width, canvas.height);
    redFilter(processedImageData.data);
    invertColors(processedImageData.data);
    thresholdFilter(processedImageData.data, level=0.33);
    return processedImageData;
}
let recordsByMatch = [];
let records = [];
let recordsByPlayer = {};
let filteredRecordsByMatch = [];
let filteredRecordsByPlayer = {};
let filteredRecords = [];

const tabNames = new Set(['stats', 'graphs', 'matches', 'parser']);

window.onload = function() {
    // Remove '#' from hash so that we can get the actual name.
    const name = window.location.hash.substring(1);
    if (tabNames.has(name)) {
      openPage(name, document.getElementById(name + 'Tab'));
    }
    else {
      // Get the element with id="defaultOpen" and click on it
      document.getElementById("defaultOpen").click();
    }

    newElement = document.getElementById('matchMap');
    maps.forEach(function (map) {
        var option = document.createElement("option");
        option.value = map;
        option.text = map;
        newElement.appendChild(option);
    });
    $.getJSON("json/records.json", function(json) {
        recordsByMatch = preprocessStats(json);
        recordsByMatch.forEach(function(record){
            record.forEach(function(entry){
                records.push(entry);
            });
        });
        recordsByPlayer = generateChartData(recordsByMatch, records);
        //filteredRecords = records.filter(elem => elem.Date > new Date(Date.now() - 14 * 24 * 60 * 60 * 1000).toJSON().slice(0,10));
        //filteredRecordsByMatch = recordsByMatch.filter(elem => elem[0].Date > new Date(Date.now() - 14 * 24 * 60 * 60 * 1000).toJSON().slice(0,10));
        filteredRecordsByMatch = recordsByMatch.slice(recordsByMatch.length-10);
        filteredRecordsByMatch.forEach(function(record){
            record.forEach(function(entry){
                filteredRecords.push(entry);
            });
        });
        filteredRecordsByPlayer = generateChartData(filteredRecordsByMatch, filteredRecords);

        //stats = generateStats(records);
        Chart.plugins.unregister(ChartDataLabels);
        $('#chartType').on('change', updateChart);
        $('#dateRange').on('change', updateChart);
        $('#scatterX').on('change', updateChart);
        $('#scatterY').on('change', updateChart);
        updateChart();
        generateMatchTable(recordsByMatch);
    });
}
