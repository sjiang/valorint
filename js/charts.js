var currentChart;

Chart.defaults.global.defaultFontFamily = "'Lato', sans-serif";
Chart.defaults.global.defaultFontSize = 16;

function updateChart() {
    if(currentChart){currentChart.destroy();}
    //$("canvas#graphsCanvas").remove();
    //$("div#canvasDiv").append('<canvas id="graphsCanvas"></canvas>');
    let ctx = document.getElementById("graphsCanvas");
    $('#scatterSelects').addClass('hide-row');
    $('#compareSelects').addClass('hide-row');
    let jsonByMatch = filteredRecordsByMatch;
    let jsonByRecords = filteredRecords;
    let jsonByPlayer = filteredRecordsByPlayer;
    let labels = Object.keys(filteredRecordsByPlayer).sort(sortLower);
    if ($("#dateRange").val() == 'filtered') {
        jsonByMatch = filteredRecordsByMatch;
        jsonByRecords = filteredRecords;
        jsonByPlayer = filteredRecordsByPlayer;
        labels = Object.keys(filteredRecordsByPlayer).sort(sortLower);
    } else {
        jsonByMatch = recordsByMatch;
        jsonByRecords = records;
        jsonByPlayer = recordsByPlayer;
        labels = Object.keys(recordsByPlayer).sort(sortLower);
    }
    
    switch ($("#chartType").val()){
        case 'combat score':
            rawCombatScores(ctx, jsonByPlayer);
            break;
        case 'kda':
            rawKDAs(ctx, jsonByPlayer);
            break;
        case 'kda per round':
            rawKDAsPerRound(ctx, jsonByPlayer);
            break;
        case 'compare stats':
            $('#compareSelects').removeClass('hide-row');
            compareStats(ctx, jsonByPlayer);
            break;
        case 'scatter plots':
            $('#scatterSelects').removeClass('hide-row');
            scatterPlots(ctx, jsonByPlayer);
            break;
        case 'match w win':
            winRatesHeatmap(ctx, parseWin(matchWinPercent(jsonByMatch, jsonByRecords, jsonByPlayer)), labels);
            break;
        case 'round w win':
            winRatesHeatmap(ctx, parseWin(roundWinPercent(jsonByMatch, jsonByRecords, jsonByPlayer)), labels);
            break;
        default:
            break;
    }
}


function rawCombatScores(ctx, jsonByPlayer){
    var boxplotData = {
        labels: Object.keys(jsonByPlayer).sort(sortLower),
        datasets: [{
            label: 'Combat Score',
            backgroundColor: 'rgba(55,134,115,.4)',
            borderColor: '#378673',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#f05c57',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Combat Scores"])
        }]
    };
    currentChart = new Chart(ctx, {
        type: 'horizontalViolin',
        data: boxplotData,
        options: {
        responsive: true,
        aspectRatio: 1.5,
        legend: {
            display:false
        },
        title: {
            display: true,
            text: 'Combat Score',
            fontSize: 20,
        },
        tooltipDecimals: 4
        }
    });
}

function rawKDAsPerRound(ctx, jsonByPlayer){
    var boxplotData = {
        labels: Object.keys(jsonByPlayer).sort(sortLower),
        datasets: [{
            label: 'Kill',
            backgroundColor: 'rgba(55,134,115,.4)',
            borderColor: '#378673',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#f05c57',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Kills per Round"])
        },
        {
            label: 'Death',
            backgroundColor: 'rgba(240, 92, 87,.4)',
            borderColor: '#f05c57',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#f05c57',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Deaths per Round"])
        },{
            label: 'Assist',
            backgroundColor: 'rgba(66, 93, 140,.4)',
            borderColor: '#425d8c',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#425d8c',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Assists per Round"])
        }]
    };
    currentChart = new Chart(ctx, {
        type: 'horizontalViolin',
        data: boxplotData,
        options: {
        responsive: true,
        aspectRatio: .7,
        legend: {
            position: 'top'
        },
        title: {
            display: true,
            text: 'Kills / Deaths / Assists per Round',
            fontSize: 20,
        },
        tooltipDecimals: 4
        }
    });
}

function rawKDAs(ctx, jsonByPlayer){
    var boxplotData = {
        labels: Object.keys(jsonByPlayer).sort(sortLower),
        datasets: [{
            label: 'Kill',
            backgroundColor: 'rgba(55,134,115,.4)',
            borderColor: '#378673',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#f05c57',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Kills"])
        },
        {
            label: 'Death',
            backgroundColor: 'rgba(240, 92, 87,.4)',
            borderColor: '#f05c57',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#f05c57',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Deaths"])
        },{
            label: 'Assist',
            backgroundColor: 'rgba(66, 93, 140,.4)',
            borderColor: '#425d8c',
            itemRadius: 2,
            itemBackgroundColor: "rgba(0,0,0,0.4)",
            itemBorderColor: "rgba(255,255,255,0.4)",
            outlierColor: '#425d8c',
            borderWidth: 1,
            data: Object.keys(jsonByPlayer).sort(sortLower).map(x => jsonByPlayer[x]["Assists"])
        }]
    };
    currentChart = new Chart(ctx, {
        type: 'horizontalViolin',
        data: boxplotData,
        options: {
        responsive: true,
        aspectRatio: .7,
        legend: {
            position: 'top'
        },
        title: {
            display: true,
            text: 'Kills / Deaths / Assists',
            fontSize: 20,
        },
        tooltipDecimals: 4
        }
    });
}

function compareStats(ctx, jsonByPlayer){
    let chartMaxMin = {
        "Combat Scores" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Combat Scores"][1] })),
            q50(records.map(o => o["Combat Score"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Combat Scores"][1] }))],
        "KDAs" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["KDAs"][1] })),
            q50(records.map(o => (o["Kill"]+o["Assist"]/3)/o["Death"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["KDAs"][1] }))],
        "Econ Ratings" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Econ Ratings"][1] })),
            q50(records.map(o => o["Econ"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Econ Ratings"][1] }))],
        "First Bloods" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["First Bloods"][1] })),
            q50(records.map(o => o["First Blood"]/o["Total Rounds"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["First Bloods"][1] }))],
        "Plants" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Plants"][1] })),
            q50(records.map(o => o["Plant"]/o["Total Rounds"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Plants"][1] }))],
        "Defuses" : [
            Math.min.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Defuses"][1] })),
            q50(records.map(o => o["Defuse"]/o["Total Rounds"])),
            Math.max.apply(Math, Object.entries(jsonByPlayer).map(function(o) { return o[1].Quartiles["Defuses"][1] }))],
    }
    let labels = ['KDAs', 'Econ Ratings', 'First Bloods', 'Plants', 'Defuses', 'Combat Scores'];
    let datasets = [];
    
    let data = [];
    labels.forEach(function(stat){
        data.push((chartMaxMin[stat][1]-chartMaxMin[stat][0])/(chartMaxMin[stat][2]-chartMaxMin[stat][0]));
    });
    datasets.push({
        backgroundColor: "rgba(128,128,128,.3)",
        data: data,
        label: 'Median',
        pointRadius: 0
    });

    Object.keys(jsonByPlayer).sort(sortLower).forEach(function(playerName, playerInd){
        let data = [];
        let dataset = {};
        labels.forEach(function(stat){
            data.push((jsonByPlayer[playerName].Quartiles[stat][1]-chartMaxMin[stat][0])/(chartMaxMin[stat][2]-chartMaxMin[stat][0]));
        });
        let playerColor = tinycolor(mpn65palette[playerInd]);
        dataset['backgroundColor'] = playerColor.setAlpha(0).toRgbString();
        dataset['pointBackgroundColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['borderColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['hoverBackgroundColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['hoverBorderColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['borderWidth'] = 1.5,
        dataset['pointRadius'] = 3;
        dataset['data'] = data;
        dataset['label'] = playerName;
        datasets.push(dataset);
    });
    let statsChartData = {datasets: datasets, labels: labels,};

    currentChart = new Chart(ctx, {
        type: 'radar',
        data: statsChartData,
        options: {
            responsive: true,
            aspectRatio: 1.2,
            legend: {
                position: 'right'
            },
            title: {
                display: true,
                text: 'Stats per Player',
                fontSize: 20,
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
    
                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        return label;
                    },
                    title: function(tooltipItem, data) {
                        return Math.round(tooltipItem[0].label*100)/100;
                    }
                }
            },
            scale: {
                angleLines: {
                    display: false
                },
                gridLines: {
                    color: 'rgba(128,128,128,0.25)'
                },
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 1,
                    backdropColor: 'rgba(255,255,255,0)'
                }
            }
        }
    });
    currentChart.data.datasets[0]._meta[Object.keys(currentChart.data.datasets[0]._meta)[0]].hidden = true;
    toggleLabels();
}

function scatterGraph(ctx, xAxis, yAxis, jsonByPlayer){
    let datasets = [];
    let labels = [];

    Object.keys(jsonByPlayer).sort(sortLower).forEach(function(playerName, playerInd){
        let data = [];
        let dataset = {};
        jsonByPlayer[playerName][xAxis].forEach(function(value, ind){
            data.push({x: jsonByPlayer[playerName][xAxis][ind],
                y: jsonByPlayer[playerName][yAxis][ind]});
        });
        let playerColor = tinycolor(mpn65palette[playerInd]);
        dataset['backgroundColor'] = playerColor.setAlpha(.7).toRgbString();
        dataset['borderColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['hoverBackgroundColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['hoverBorderColor'] = playerColor.setAlpha(1).toRgbString();
        dataset['borderWidth'] = 1.5,
        dataset['pointRadius'] = 3;
        dataset['data'] = data;
        dataset['label'] = playerName;
        datasets.push(dataset);
        labels.push(playerName);
    });
    let scatterChartData = {datasets: datasets, labels: labels,};
    currentChart = new Chart.Scatter(ctx, {
        data: scatterChartData,
        options: {
            responsive: true,
            aspectRatio: 1.2,
            legend: {
                display:true,
                position:'right',
                labels: {
                    fontSize: 14,
                },
            },
            title: {
                display: true,
                text: xAxis + ' vs ' + yAxis,
                fontSize: 20,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: yAxis,
                        fontSize: 18,
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: xAxis,
                        fontSize: 18,
                    }
                }]
            },
            tooltipDecimals: 4,
            tooltips: {
                //mode: 'nearest',
                //intersect: false,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.labels[tooltipItem.datasetIndex];
                        return label + ': (' + tooltipItem.xLabel.toFixed(2) + ', ' + tooltipItem.yLabel.toFixed(2) + ')';
                    }
                },
            },
        }
    });
}

function scatterPlots(ctx, jsonByPlayer) {
    let xAxis = $("#scatterX").val();
    let yAxis = $("#scatterY").val();
    scatterGraph(ctx, xAxis, yAxis, jsonByPlayer);
}

function matchWinPercent(jsonByMatch, jsonByRounds, jsonByPlayer){
    matchWin = {};
    Object.keys(jsonByPlayer).forEach(function(player1){
        Object.keys(jsonByPlayer).forEach(function(player2){
            if (!matchWin.hasOwnProperty(player1)){
                matchWin[player1] = {};
            }
            matchWin[player1][player2] = {Won: 0, Total: 0};
            if (player1 == player2){
                matchWin[player1][player2] = {
                    Won: jsonByRounds.filter(elem => elem.Player==player1 && elem["Rounds Won"] == elem["Winning Rounds"]).map(function(item){return item["Rounds Won"]}).length, 
                    Total: jsonByRounds.filter(elem => elem.Player==player1).map(function(item){return item["Rounds Won"]}).length}
            }
        });
    })

    jsonByMatch.forEach(function(match) {
        let winningTeam = match.filter(elem => elem["Rounds Won"] == elem["Winning Rounds"]).map(elem=>elem.Player);
        let losingTeam = match.filter(elem => elem["Rounds Won"] != elem["Winning Rounds"]).map(elem=>elem.Player);
        [].concat.apply([], winningTeam.pairwise()).forEach(function (pair){
            matchWin[pair[0]][pair[1]].Won += 1;
            matchWin[pair[0]][pair[1]].Total += 1;
        });
        [].concat.apply([], losingTeam.pairwise()).forEach(function (pair){
            matchWin[pair[0]][pair[1]].Total += 1;
        });
    })

    return matchWin;
}

function roundWinPercent(jsonByMatch, jsonByRounds, jsonByPlayer){
    roundWin = {};
    Object.keys(jsonByPlayer).forEach(function(player1){
        Object.keys(jsonByPlayer).forEach(function(player2){
            if (!roundWin.hasOwnProperty(player1)){
                roundWin[player1] = {};
            }
            roundWin[player1][player2] = {Won: 0, Total: 0};
            if (player1 == player2){
                roundWin[player1][player2] = {
                    Won: sum(jsonByRounds.filter(elem => elem.Player==player1).map(function(item){return item["Rounds Won"]})), 
                    Total: sum(jsonByRounds.filter(elem => elem.Player==player1).map(function(item){return item["Total Rounds"]}))}
            }
        });
    })

    jsonByMatch.forEach(function(match) {
        let winningTeam = match.filter(elem => elem["Rounds Won"] == elem["Winning Rounds"]).map(elem=>elem.Player);
        let losingTeam = match.filter(elem => elem["Rounds Won"] != elem["Winning Rounds"]).map(elem=>elem.Player);
        [].concat.apply([], winningTeam.pairwise()).forEach(function (pair){
            roundWin[pair[0]][pair[1]].Won += match[0]["Winning Rounds"];
            roundWin[pair[0]][pair[1]].Total += match[0]["Total Rounds"];
        });
        [].concat.apply([], losingTeam.pairwise()).forEach(function (pair){
            roundWin[pair[0]][pair[1]].Won += match[0]["Total Rounds"] - match[0]["Winning Rounds"];
            roundWin[pair[0]][pair[1]].Total += match[0]["Total Rounds"];
        });
    })

    return roundWin;
}

function parseWin(winRate){
    out = [];
    Object.keys(winRate).sort().forEach(function (playerX){
        Object.keys(winRate[playerX]).sort().forEach(function (playerY){
            out.push({
                x: playerX,
                y: playerY,
                v: winRate[playerX][playerY].Total == 0? -1: winRate[playerX][playerY].Won/winRate[playerX][playerY].Total,
                d: winRate[playerX][playerY].Won + '/' + winRate[playerX][playerY].Total
            });
        });
    });
    return out;
}

function winRatesHeatmap(ctx, data, labels){
    currentChart = new Chart(ctx, {
        plugins: [ChartDataLabels],
        type: 'matrix',
        data: {
            datasets: [{
                label: 'My Matrix',
                data: data,
                backgroundColor: function(ctx) {
                    var value = ctx.dataset.data[ctx.dataIndex].v;
                    var alpha = value;
                    return tolRainbowColor(value).toRgbString();
                },
                borderColor: function(ctx) {
                    var value = ctx.dataset.data[ctx.dataIndex].v;
                    var alpha = value;
                    return tolRainbowColor(value).toRgbString();
                },
                borderWidth: 1,
                borderSkipped: false,
                hoverBorderColor: undefined,
                hoverBackgroundColor: undefined,
                width: function(ctx) {
                    var a = ctx.chart.chartArea;
                    return (a.right - a.left) / labels.length;
                },
                height: function(ctx) {
                    var a = ctx.chart.chartArea;
                    return (a.bottom - a.top) / labels.length;
                }
            }]
        },
        options: {
            aspectRatio: 1,
            legend: {
                display: false
            },
            tooltips: {
                displayColors: false,
                callbacks: {
                    title: function() { return '';},
                    label: function(item, data) {
                        var v = data.datasets[item.datasetIndex].data[item.index];
                        return [(v.x == v.y? v.x: v.x +" + "+ v.y), v.v == -1? 'N/A' : (v.v*100).toFixed(2) + ': ' + v.d];
                    }
                }
            },
            scales: {
                xAxes: [{
                    offset: true,
                    type: 'category',
                    position: 'top',
                    labels: labels,
                    gridLines: {
                        display: false,
                        drawBorder: false,
                        tickMarkLength: 0
                    },
                    ticks : {
                        minRotation : 45,
                        padding: 15,
                    }
                }
                ],
                yAxes: [{
                    offset: true,
                    type: 'category',
                    position: 'left',
                    labels: labels,
                    gridLines: {
                        display: false,
                        drawBorder: false,
                        tickMarkLength: 0
                    },
                    ticks: {
                        padding: 10
                    }
                }]
            },
            plugins: {
                datalabels: {
                    formatter: function(value, context) {
                        return value.v == -1 ? "N/A" : (value.v*100).toFixed(1);
                    }
                }
            }
        }
    });
}


function sortLower(a,b){
    return a.toLowerCase().localeCompare(b.toLowerCase());
}

function toggleLabels(skip = false) {
    currentChart.data.datasets.forEach(function(ds, ind) {
        if (skip && ind == 0){
            return;
        } else {
            ds._meta[Object.keys(ds._meta)[0]].hidden = ds._meta[Object.keys(ds._meta)[0]].hidden? null:true;
        }
    });
    currentChart.update();
};